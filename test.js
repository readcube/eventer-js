const Eventer = require('./index');

const obj = Eventer({});

obj.on('test', (e, data) => {
  console.log('handle on', e, data);
});

obj.one('test', (e, data) => {
  console.log('handle one', e, data);
});

obj.trigger('test', [{ name: 'hello 1' }]);
obj.trigger('test', [{ name: 'hello 2' }]);
obj.trigger('test', [{ name: 'hello 3' }]);

console.log('handler count', obj.handlerCount('test'));
