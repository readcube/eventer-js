const splitter = /\s+/;

function Eventer(target) {
  let store = {};

  // use prototype if object is a function
  target = typeof target === 'function' ? target.prototype : target;

  // Returns handle needed for ignore function
  target.on = target.bind = function(event, handler) {
    event.split(splitter).forEach(key => {
      store[key] = store[key] || [];
      store[key].push(handler);
    });
    return this;
  };

  target.one = function(event, handler) {
    event.split(splitter).forEach(key => {
      let handleOnce = (e, data) => {
        let index = store[key].findIndex(h => h === handleOnce);
        store[key].splice(index, 1);
        handler.call(this, e, data);
      };
      store[key] = store[key] || [];
      store[key].push(handleOnce);
    });
    return this;
  };

  target.off = target.unbind = function(event, handler) {
    // Remove all listeners if nothing was passed
    if (!event) {
      store = {};
      return this;
    };

    event.split(splitter).forEach(key => {
      if (!store[key]) return
      let handlers = store[key];
      let i = handlers.length;
      while (i--) {
        if (!handler || handler === handlers[i - 1]) {
          handlers.splice(i - 1, 1);
        }
      }
    });
    return this;
  };

  target.trigger = target.fire = function(event, data) {
    event.split(splitter).forEach(key => {
      if (store[key]) {
        let evtobj = {};
        if (typeof Event !== 'undefined') {
          evtobj = new Event(key);
        }
        let handlers = store[key];
        let i = handlers.length;
        while (i--) {
          if (typeof handlers[i] === 'function') {
            handlers[i].call(this, evtobj, ...(data || []));
          }
        }
      }
    });
    return this;
  };

  target.handlerCount = function(event) {
    return store[event].length || 0;
  };

  return target;
};

export default Eventer;
